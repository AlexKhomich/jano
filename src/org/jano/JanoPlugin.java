package org.jano;

import fi.iki.elonen.NanoHTTPD;
import org.jano.resources.ResourceSystem;

/**
 * Server extension
 */
public interface JanoPlugin {

	/**
	 * Checks can this plugin serve next uri
	 * @param uri
	 * @return <c>true</c> if plugin can process such sort of URIs
	 */
	public boolean canServe(String uri, NanoHTTPD.Method method);

	/**
	 * Serves request in context of resource system
	 * @param session current request session
	 * @param resourceSystem resource system of server
	 */
	public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session, ResourceSystem resourceSystem) throws Exception;
}
