package org.jano.util;

public class StringUtils {

	public static String pad(String text, int width) {
		int toPad = width - text.length();
		return text + new String(new char[toPad]).replace('\0', ' ');
	}
}
