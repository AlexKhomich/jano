package org.jano.util;

import sun.plugin.dom.exception.InvalidStateException;

public class Optional<T> {
	private final T ref;

	private Optional(T ref) {
		this.ref = ref;
	}

	public T get() {
		if (null == ref) throw new InvalidStateException("Optional is empty");
		return ref;
	}

	public T getOrNull() {
		return ref;
	}

	public T getOrElse(T other) {
		return null != ref ? ref : other;
	}

	public boolean isPresent() {
		return null != ref;
	}

	public boolean isEmpty() {
		return null == ref;
	}

	public static <A> Optional<A> any(A ref) {
		return new Optional(ref);
	}

	public static <A> Optional<A> of(A ref) {
		if (null == ref) throw new NullPointerException("ref");
		return any(ref);
	}

	public static <A> Optional<A> empty() {
		return any(null);
	}
}
