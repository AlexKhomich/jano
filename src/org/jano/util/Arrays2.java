package org.jano.util;

import java.lang.reflect.Array;

public class Arrays2 {

	/**
	 * Creates an array of length = 1 with element set at index 0
	 */
	public static <T> T[] single(T element) {
		if (null == element) throw new NullPointerException();

		T[] array = (T[])Array.newInstance(element.getClass(), 1);
		array[0] = element;
		return array;
	}

	/**
	 * Prepends element to an existing array
	 */
	public static <T> T[] prepend(T element, T[] array) {
		if (null == element) throw new NullPointerException();

		T[] newArray = (T[])Array.newInstance(element.getClass(), array.length - 1);
		System.arraycopy(array, 0, newArray, 1, array.length);
		newArray[0] = element;
		return newArray;
	}
}
