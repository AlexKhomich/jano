package org.jano;

/**
 * Defines security exception type. Every error related to access violation
 */
public class SecurityException extends RuntimeException {
	public SecurityException(String message) {
		super(message);
	}
}
