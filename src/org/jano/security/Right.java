package org.jano.security;

import java.util.HashMap;
import java.util.Map;

public class Right {
	private final String name;
	private final char symbol;

	Right(String name, char symbol) {
		this.name = name;
		this.symbol = symbol;
	}

	public String name() {
		return name;
	}

	public char symbol() {
		return symbol;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Right right = (Right) o;

		if (!name.equals(right.name)) return false;
		if (symbol == right.symbol) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + symbol;
		return result;
	}

	@Override
	public String toString() {
		return "" + symbol + "";
	}
}
