package org.jano.security;

import org.jano.resources.ResourceRef;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Security wrapper for resource ref
 */
public class SecuredRef implements ResourceRef {
	private final ResourceRef ref;
	private final SecurityContext securityContext;

	public SecuredRef(ResourceRef ref, SecurityContext securityContext) {
		this.ref = ref;
		this.securityContext = securityContext;
	}

	private void checkAllowed(Right right) {
		if (!securityContext.isAllowed(right)) {
			throw new org.jano.SecurityException("Operation " + right + " is not permitted with " + this.toString());
		}
	}

	@Override
	public String name() {
		return ref.name();
	}

	@Override
	public String uri() {
		return ref.uri();
	}

	@Override
	public boolean exists() {
		return ref.exists();
	}

	@Override
	public boolean isBrowsable() {
		return ref.isBrowsable();
	}

	@Override
	public ResourceRef[] browse() {
		checkAllowed(Rights.browse);
		return ref.browse();
	}

	@Override
	public ResourceRef child(String name) {
		return ref.child(name);
	}

	@Override
	public boolean isMaterial() {
		return ref.isMaterial();
	}

	@Override
	public Long length() {
		if (securityContext.isAllowed(Rights.browse)) {
			return ref.length();
		} else {
			return null;
		}
	}

	@Override
	public ResourceRef parent() {
		return ref.parent();
	}

	@Override
	public InputStream inputStream() {
		return ref.inputStream();
	}

	@Override
	public OutputStream outputStream(boolean append) {
		checkAllowed(Rights.write);
		if (append) { checkAllowed(Rights.append); }
		return ref.outputStream(append);
	}

	@Override
	public boolean delete() {
		checkAllowed(Rights.delete);
		return ref.delete();
	}

	@Override
	public String toString() {
		return "SecuredRef [" + ref.toString() + "]";
	}
}
