package org.jano.security;

import org.jano.resources.ResourceRef;
import org.jano.resources.ResourceSystem;
import org.jano.resources.ResourceView;

import java.util.Set;

/**
 * Jano Security point
 */
public final class Security {

	/**
	 * Creates security context from the set of allowed rights
	 */
	private static SecurityContext createContext(final Set<Right> rights) {
		return new SecurityContext() {
			@Override
			public boolean isAllowed(Right right) {
				return rights.contains(right);
			}
		};
	}

	/**
	 * Secures the resource view with set of allowed rights
	 * @param resourceView the view to secure
	 * @param rights set of the rights allowed to execute under rights
	 * @return
	 */
	public static ResourceView secure(final ResourceView resourceView, final Set<Right> rights) {
		final SecurityContext context = createContext(rights);
		return new ResourceView() {
			@Override
			public ResourceRef viewOf(String viewUri, String childUri, ResourceSystem system) {
				ResourceRef ref = resourceView.viewOf(viewUri, childUri, system);
				return new SecuredRef(ref, context);
			}
		};
	}
}
