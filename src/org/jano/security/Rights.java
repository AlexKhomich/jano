package org.jano.security;

import java.util.*;

/**
 * Set of supported server rights
 */
public final class Rights {
	private static Map<Character, Right> symbolIndex = new HashMap<Character, Right>();

	public static final Right read = declare("read", 'r');
	public static final Right write = declare("write", 'w');
	public static final Right delete = declare("delete", 'd');
	public static final Right append = declare("append", 'a');
	public static final Right browse = declare("browse", 'b');

	public static final Set<Right> all = Collections.unmodifiableSet(new HashSet<Right>() {{
		add(read);
		add(write);
		add(delete);
		add(append);
		add(browse);
	}});

	public static Set<Right> of(Right ... rights) {
		HashSet<Right> result = new HashSet<Right>();
		for (Right right : rights) {
			result.add(right);
		}
		return result;
	}

	public static Right declare(String name, char symbol) {
		Right right = new Right(name, symbol);
		symbolIndex.put(symbol, right);
		return right;
	}


	public static Right bySymbol(char symbol) {
		return symbolIndex.get(symbol);
	}
}
