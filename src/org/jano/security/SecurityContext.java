package org.jano.security;

/**
 * Introduces a security context which allows to identify rights to execute some operation
 */
public interface SecurityContext {
	/**
	 * Checks can this context execute this right
	 * @param right to check
	 * @return <c>true</c> if right is allowed in current security context
	 */
	boolean isAllowed(Right right);
}
