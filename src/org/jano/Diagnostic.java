package org.jano;

/**
 * Encapsulation of diagnostic printing into one place
 */
public class Diagnostic {

	public static void log(String message) {
		System.out.println(message);
	}

	public static void log(String message, Exception e) {
		log(message);
		log(e);
	}

	public static void log(Exception e) {
		e.printStackTrace();
	}

}
