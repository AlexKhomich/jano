package org.jano;

/**
 * Exceptional situation happened in Jano server lifecycle.
 */
public class JanoException extends RuntimeException {

	public JanoException(String message) {
		super(message);
	}

	public JanoException(String message, Throwable cause) {
		super(message, cause);
	}

	public JanoException(Throwable cause) {
		super(cause);
	}
}
