package org.jano;

import org.jano.security.Rights;

import java.io.File;

/**
 * Bootstrap for JANO web server
 * We can configure server from command line using following parameters
 * -p | --port {port}
 * -h | --host {host}
 * -m | --mount {point} {resource} [rights]  (:) available multiple times
 *
 * {port} - any free system porn number
 * {host} - force server to serve only request for specific host
 * {point} - root-based uri which will server resource
 * {resource} - original resource location
 * {rights} - set of flags describing allowed operations with this resource, or wildcard e.g. *, rw, rb ...
 */
public class Bootstrap {
	public static void main(String[] args) {
		//let us know what is the working dir
		Diagnostic.log("Working Dir: " + new File("").getAbsolutePath());

		//create configuration
		JanoConfig config = new JanoConfig();

		//parse command lines
		config = new CommandLineParser(config).parse(args);

		//create and launch server
		config
			.createServer()
			.start()
			.await();
	}
}
