package org.jano;

import java.util.regex.Pattern;

/**
 * Some helper functions to work with root-based URIs
 * Absolute root-based uri starts with /, relative with any other character
 * Valid internal uri:
 * 1. Doesn't end with slash (/)
 * 2. Doesn't contain backward slash (\)
 * 3. Doesn't contain multiple slashes in row (///)
 */
public final class Uri {
	public static final char PATH_SEPARATOR_CHAR = '/';
	public static final char ALTERNATE_PATH_SEPARATOR_CHAR = '\\';
	public static final String PATH_SEPARATOR = String.valueOf(new char[] { PATH_SEPARATOR_CHAR });
	public static final String ROOT = PATH_SEPARATOR;
	public static final String DBL_PATH_SEPARATOR = PATH_SEPARATOR + PATH_SEPARATOR;
	public static final String BACK_PATH_SEPARATOR = "\\";

	private static final Pattern DBL_SLASH_PATTERN = Pattern.compile("[/]{2,}");

	/**
	 * Joins two URIs (parent and child one) into one
	 * @return new uri built from parent and child
	 */
	public static String of(String parent, String child) {
		return of(parent + PATH_SEPARATOR + child);
	}

	/**
	 * Do normalization of "dirty" uri into valid URI.
	 * @param uri
	 * @return
	 */
	public static String of(String uri) {
		return 	removeTrailingSlash(fixMultipleSlashes(fixAlternateSlash(uri)));
	}

	private static String fixAlternateSlash(String uri) {
		return uri.replace(ALTERNATE_PATH_SEPARATOR_CHAR, PATH_SEPARATOR_CHAR);
	}

	private static String fixMultipleSlashes(String uri) {
		return DBL_SLASH_PATTERN.matcher(uri).replaceAll(PATH_SEPARATOR);
	}

	private static String removeTrailingSlash(String uri) {
		return uri.equals(ROOT) ? ROOT : (uri.endsWith(PATH_SEPARATOR) ? uri.substring(0, uri.length() - 1) : uri);
	}

	/**
	 * Provides the parent root-based uri
	 */
	public static String parent(String uri) {
		uri = of(uri);
		int slashIndex = uri.lastIndexOf(PATH_SEPARATOR);
		if (slashIndex > 0) {
			return uri.substring(0, slashIndex);  //take to the first /A/B/C -> /A/B
		} else if (slashIndex == 0) {
			return ROOT;
		} else {
			return uri;
		}
	}
}
