package org.jano;


public class JsonModel {
	public static class ResourceRef {
		public String name;
		public String uri;
		public boolean isBrowsable;
		public boolean isMaterial;
		public Long length;
	}

	public static class ErrorModel {
		public static Integer errorCode;
		public String errorMessage;
	}

	public static ResourceRef build(org.jano.resources.ResourceRef ref) {
		ResourceRef model = new ResourceRef();
		model.name = ref.name();
		model.uri = ref.uri();
		model.isBrowsable = ref.isBrowsable();
		model.isMaterial = ref.isMaterial();
		model.length = ref.length();
		return model;
	}

	public static ErrorModel build(Throwable t) {
		ErrorModel model = new ErrorModel();
		model.errorMessage = t.getMessage();
		return model;
	}
}
