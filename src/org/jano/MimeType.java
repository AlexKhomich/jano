package org.jano;

import java.util.HashMap;

public class MimeType {
	public static final String TEXT = "text/plain";
	public static final String JS = "text/javascript";
	public static final String JSON = "application/json";
	public static final String HTML = "text/html";
	public static final String CSS = "text/css";
	public static final String GIF = "image/gif";
	public static final String PNG = "image/png";
	public static final String JPEG = "image/jpeg";
	public static final String SVG = "image/svg+xml";
	public static final String DEFAULT = TEXT;

	private static final HashMap<String, String> map = new HashMap<String, String>();

	static {
		map.put("htm", HTML);
		map.put("html", HTML);
		map.put("js", JS);
		map.put("json", JSON);
		map.put("css", CSS);
		map.put("gif", GIF);
		map.put("png", PNG);
		map.put("jpg", JPEG);
		map.put("jpe", JPEG);
		map.put("jpeg", JPEG);
		map.put("svg", SVG);
	}

	public static String of(String uri) {
		int lastDot = uri.lastIndexOf(".");
		if (lastDot >= 0) {
			String mimeType = map.get(uri.substring(lastDot + 1).toLowerCase());
			if (null != mimeType) {
				return mimeType;
			}
		}
		return DEFAULT;
	}
}
