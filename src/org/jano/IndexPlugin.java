package org.jano;

import fi.iki.elonen.NanoHTTPD;
import org.jano.resources.ResourceSystem;

/**
 * Serves ROOT uri, redirecting to URI
 */
public class IndexPlugin implements JanoPlugin {
	private static final String indexUri = "/index.html";

	@Override
	public boolean canServe(String uri, NanoHTTPD.Method method) {
		return uri.equals(Uri.ROOT) && method == NanoHTTPD.Method.GET;
	}

	@Override
	public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session, ResourceSystem resourceSystem) throws Exception {
		NanoHTTPD.Response response = Reply.with(NanoHTTPD.Response.Status.REDIRECT, null, "");
		response.addHeader("Location", indexUri);
		return response;
	}
}
