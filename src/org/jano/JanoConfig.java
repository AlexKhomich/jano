package org.jano;

import org.jano.resources.FileResourceView;
import org.jano.resources.ResourceView;
import org.jano.security.Right;
import org.jano.security.Security;

import java.util.*;

/**
 * JANO Server configurator
 */
public class JanoConfig {
	public static final int DEFAULT_PORT = 8080;

	private String hostname = null;
	private int port = DEFAULT_PORT;
	private HashMap<String, ResourceView> mountPoints = new HashMap<String, ResourceView>();

	/**
	 * Hostname configuration
	 */
	public String hostname() {
		return hostname;
	}

	/**
	 * Configures a restriction to listen only requests to specified hostname.
	 * Default is <c>null</c> - means no restriction
	 */
	public JanoConfig hostname(String hostname) {
		Diagnostic.log("Hostname: " + hostname);
		this.hostname = hostname;
		return this;
	}

	/**
	 * Port which Jano server will listen
	 */
	public int port() {
		return port;
	}

	/**
	 * Set a port to listen by Jano HTTP server
	 */
	public JanoConfig port(int port) {
		Diagnostic.log("Port: " + port);
		this.port = port;
		return this;
	}

	/**
	 * Add resource mount to the server config.
	 * By default resource original path will be taken as mount point
	 * @param uri mount point for file system resource view
	 * @param path real host filesystem path to be a root of mounted point
	 */
	public JanoConfig mountFileView(String uri, String path, Set<Right> rights) {
		Diagnostic.log("Mount: " + path + " @ " + uri + " " + rights);
		path = path.startsWith(Uri.ROOT) ? "." + path : path;	//use relative path
		return mountView(uri, Security.secure(new FileResourceView(path), rights));
	}

	/**
	 * Add resource mount to the server config.
	 * By default resource original path will be taken as mount point
	 * @param uri mount point for resource view
	 * @param view existing instance of the file view
	 */
	public JanoConfig mountView(String uri, ResourceView view) {
		uri = Uri.of(uri);
		if (mountPoints.containsKey(uri)) {
			throw new JanoException("Unable to mount '" + uri + "' second time");
		}

		mountPoints.put(uri, view);
		return this;
	}

	/**
	 * Provides an index of registered mount points
	 */
	public Map<String, ResourceView> mountPoints() {
		return Collections.unmodifiableMap(mountPoints);
	}

	/**
	 * Creates an new Jano server instance
	 */
	public JanoServer createServer() {
		return new JanoServer(this);
	}


}
