package org.jano;

import com.google.gson.Gson;
import fi.iki.elonen.NanoHTTPD;
import org.jano.resources.ResourceRef;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Reply {

	public static String json(Object any) {
		if (null != any) {
			return new Gson().toJson(any);
		} else {
			return "";
		}
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status) {
		return with(status, MimeType.TEXT, "");
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status, String mimeType, String text) {
		return new NanoHTTPD.Response(status, mimeType,text);
	}

	public static NanoHTTPD.Response with(String mimeType, String text) {
		return with(NanoHTTPD.Response.Status.OK, mimeType, text);
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status, ResourceRef ref) {
		return with(status, MimeType.JSON, json(JsonModel.build(ref)));
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status, Object result) {
		return with(status, MimeType.JSON, json(result));
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status, Exception e) {
		StringWriter buffer = new StringWriter();
		PrintWriter writer = new PrintWriter(buffer);
		e.printStackTrace(writer);
		writer.close();
		return with(status, MimeType.TEXT, buffer.toString());
	}

	public static NanoHTTPD.Response with(NanoHTTPD.Response.Status status, String mimeType, byte[] content) {
		return new NanoHTTPD.Response(status, mimeType, new ByteArrayInputStream(content));
	}
}
