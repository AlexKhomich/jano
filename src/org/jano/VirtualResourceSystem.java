package org.jano;

import org.jano.resources.ResourceException;
import org.jano.resources.ResourceRef;
import org.jano.resources.ResourceSystem;
import org.jano.resources.ResourceView;

import java.util.Map;

public class VirtualResourceSystem implements ResourceSystem {

	private final Map<String, ResourceView> mountPoints;

	public VirtualResourceSystem(Map<String, ResourceView> mountPoints) {
		this.mountPoints = mountPoints;
	}

	/**
	 * Binds resource to specified URI
	 * @param resourceUri uri
	 */
	@Override
	public ResourceRef get(String resourceUri) {
		String uri = resourceUri;
		while (!uri.isEmpty()) {
			ResourceView view = mountPoints.get(uri);
			if (null != view) {
				return view.viewOf(uri, resourceUri.substring(uri.length()), this);
			}
			//just take another part
			int nextSeparator = uri.lastIndexOf(Uri.PATH_SEPARATOR_CHAR);
			uri = uri.substring(0, nextSeparator);
		}

		ResourceView view = mountPoints.get(Uri.ROOT);
		if (null != view) {
			return view.viewOf(Uri.ROOT, resourceUri, this);
		}

		throw new ResourceException("Resource URI '" + resourceUri + "' has no known views");
	}
}
