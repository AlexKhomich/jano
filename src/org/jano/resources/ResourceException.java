package org.jano.resources;

/**
 * Represents all exceptional situations related to resource management
 */
public class ResourceException extends RuntimeException {

	public ResourceException(String message) {
		super(message);
	}

	public ResourceException(Throwable cause) {
		super(cause);
	}

	public ResourceException(String message, Throwable cause) {
		super(message, cause);
	}
}
