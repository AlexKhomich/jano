package org.jano.resources;

import java.io.InputStream;
import java.io.OutputStream;

/**
 *  Abstract Resource Reference.
 *  It is a part of VRS which allows flexible, transparent and secure resource operations...
 */
public interface ResourceRef {

	/**
	 * Provides a short resource name (relative to its parent)
	 */
	String name();

	/**
	 * Provides root based resource URI which can be used to refer it.
	 */
	String uri();

	/**
	 * Provides a parent resource.
	 * @returns parent resource or itself if current resource is root one
	 */
	ResourceRef parent();

	/**
	 * Checks is resource really exists
	 */
	boolean exists();

	/**
	 * Resource is rather container (directory) for other resources
	 */
	boolean isBrowsable();

	/**
	 * Browses underlying resources. Applies only to browsable resources.
	 * Material resources will return empty array?..
	 */
	ResourceRef[] browse();


	/**
	 * Provides resource reference to the child resource with specified name.
	 * @param name name of child
	 * @return child resource ref or <c>null</c> if this resource fully sure it can't have children
	 */
	ResourceRef child(String name);

	/**
	 * Provides attribute that current resource is a material and contains some content.
	 */
	boolean isMaterial();

	/**
	 * Length of resource content in bytes (if it is material and there is ability to check it size)
	 * @return size of resource or <c>null</c> if not possible to determine length of the content
	 */
	Long length();

	/**
	 * Opens a new input stream for reading resource content
	 */
	public InputStream inputStream();

	/**
	 * Opens a new output stream to store resource content
	 * @param append set in <c>true</c> if you want to append content to the existing content
	 */
	public OutputStream outputStream(boolean append);

	/**
	 * Deletes material resource
	 * @return <c>true</c> if resource has been deleted, <c>false</c> if there are any other reason than security prevents removal
	 */
	public boolean delete();
}
