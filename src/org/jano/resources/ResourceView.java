package org.jano.resources;

/**
 * Provides an abstraction of resource view for any sort of resource storage/provider
 */
public interface ResourceView {

	/**
	 * Provides a resource reference within current view
	 * @param viewUri an uri where this resource view is mounted
	 * @param childUri child uri relative to the viewUri
	 * @param system resource system requested this view
	 * @return a reference to the resource
	 */
	ResourceRef viewOf(String viewUri, String childUri, ResourceSystem system);
}
