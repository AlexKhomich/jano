package org.jano.resources;

import org.jano.Uri;

import java.io.File;

/**
 * Implements simple java.io.File based resource view
 */
public class FileResourceView implements ResourceView {
	private final File baseDir;

	public FileResourceView(String baseDir) {
		this.baseDir = new File(baseDir);
	}

	@Override
	public ResourceRef viewOf(String viewUri, String childUri, ResourceSystem system) {
		return new FileResourceRef(
			new File(baseDir, childUri),
		    Uri.of(viewUri, childUri),
		    system
	    );
	}
}
