package org.jano.resources;

import java.io.*;

/**
 * Some helper classes to manage contents
 */
public class Resource {
	// buffer size used for reading and writing
	private static final int BUFFER_SIZE = 8192;

	public static byte[] get(ResourceRef ref) {
		InputStream input = ref.inputStream();
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			copy(input, output);
			return output.toByteArray();
		} catch (IOException e) {
			throw new ResourceException(e);
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				throw new ResourceException(e);
			}
		}
	}

	public static void put(ResourceRef ref, byte[] content, boolean append) {
		OutputStream output = ref.outputStream(append);
		try {
			InputStream input = new ByteArrayInputStream(content);
			copy(input, output);
		} catch (IOException e) {
			throw new ResourceException(e);
		} finally {
			try {
				output.close();
			} catch (IOException e) {
				throw new ResourceException(e);
			}
		}
	}

	/**
	 * Reads all bytes from an input stream and writes them to an output stream.
	 */
	private static long copy(InputStream source, OutputStream sink) throws IOException {
		long nread = 0L;
		byte[] buf = new byte[BUFFER_SIZE];
		int n;
		while ((n = source.read(buf)) > 0) {
			sink.write(buf, 0, n);
			nread += n;
		}
		return nread;
	}
}
