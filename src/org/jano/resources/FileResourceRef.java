package org.jano.resources;

import org.jano.Uri;

import java.io.*;

/**
 * Resource reference implementation for regular IO file
 */
public class FileResourceRef implements ResourceRef {
	private final File file;
	private final String uri;
	private final ResourceSystem resourceSystem;

	public FileResourceRef(File file, String uri, ResourceSystem resourceSystem) {
		this.file = file;
		this.uri = uri;
		this.resourceSystem = resourceSystem;
	}

	@Override
	public boolean exists() {
		return file.exists();
	}

	@Override
	public String name() {
		return file.getName();
	}

	@Override
	public String uri() {
		return uri;
	}

	@Override
	public ResourceRef parent() {
		return resourceSystem.get(Uri.parent(uri));
	}

	@Override
	public boolean isBrowsable() {
		return exists() && file.isDirectory();
	}

	@Override
	public ResourceRef[] browse() {
		File[] files = file.listFiles();
		ResourceRef[] refs = new ResourceRef[files.length];

		for (int n = 0; n < files.length; ++n) {
			File file = files[n];
			refs[n] = resourceSystem.get(Uri.of(uri, file.getName()));
		}

		return refs;
	}

	@Override
	public ResourceRef child(String name) {
		return resourceSystem.get(Uri.of(uri, name));
	}

	@Override
	public boolean isMaterial() {
		return exists() && !file.isDirectory();
	}

	@Override
	public Long length() {
		return isMaterial() ? file.length() : null;
	}

	@Override
	public InputStream inputStream() {
		try {
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new ResourceException(e);
		}
	}

	@Override
	public OutputStream outputStream(boolean append) {
		try {
			file.getParentFile().mkdirs();	//we should ensure all directories has been created...
			return new FileOutputStream(file, append);
		} catch (FileNotFoundException e) {
			throw new ResourceException(e);
		}
	}

	@Override
	public boolean delete() {
		return file.delete();
	}

	@Override
	public String toString() {
		return "FileRef [uri=" + uri() + ", path=" + file.getPath() + "]";
	}
}
