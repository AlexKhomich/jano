package org.jano.resources;

/**
 * Abstract resource system. Single point to obtain resources by their url
 */
public interface ResourceSystem {

	/**
	 * Provides reference to the resource based
	 * @param uri absolute (root-related) uri of the resource
	 * @return resource reference
	 */
	ResourceRef get(String uri);
}
