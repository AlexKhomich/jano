package org.jano;

public enum Access {
	BROWSE,
	GET,
	CREATE,
	UPDATE,
	DELETE,
	READ(GET, BROWSE),
	WRITE(CREATE, UPDATE),
	ALL(READ, WRITE, DELETE);

	public int mask;

	private Access() {
		mask = 1 << nextShift();
	}

	private Access(Access a, Access b) {
		mask = a.mask | b.mask;
	}

	private Access(Access a, Access b, Access c) {
		mask = a.mask | b.mask | c.mask;
	}

	private static int shift = 0;

	private static int nextShift() {
		return shift++;
	}

	public boolean can(int flags) {
		return 0 != (flags & mask);
	}
}
